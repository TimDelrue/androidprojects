package com.example.U2_PW2_Grid_Project.data

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Topic(
    @StringRes val textId : Int,
    val number : Int,
    @DrawableRes val imgId: Int
)
