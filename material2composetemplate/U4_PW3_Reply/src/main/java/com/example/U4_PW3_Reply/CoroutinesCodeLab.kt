package com.example.U4_PW3_Reply

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlin.system.measureTimeMillis

class CoroutinesCodeLab {

}

fun main() {

    var time = measureTimeMillis {
        // synchronous
        runBlocking {
            println("Weather forecast")
            printForecast()
            printTemperature()
        }
    }
    println("Execution time: ${time / 1000.0} seconds")
    println()
    time = measureTimeMillis {
        // asynchronous (because of the "launch"es
        runBlocking {
            println("Weather forecast")
            // launch is used to run asynchronous (suspend) methods
            // "fire and forget"
            launch {
                printForecast()
            }
            val job = launch { // launch returns a job, job.cancel() is an option
                printTemperature()
            }
            println("Have a good day!")
            // jobs
            // child job killed -> death
            // parent job killed -> death + all kids death
            // child job killed with exception -> exception to parent
        }
    }
    println("Execution time: ${time / 1000.0} seconds")
    println()

    runBlocking {
        println("Weather forecast")
        val forecast: Deferred<String> = async {
            getForecast()
        }
        val temperature: Deferred<String> = async {
            getTemperature()
        }
        println("${forecast.await()} ${temperature.await()}")
        println("Have a good day!")
    }

    println()

    runBlocking {
        println("Weather forecast")
        println(getWeatherReport())
        println("Have a good day!")
    }

    println()

    runBlocking {
        println("Weather forecast")
        try {
            println(getWeatherReportWithError())
        } catch (e: AssertionError) {
            println("Caught exception in runBlocking(): $e")
            println("Report unavailable at this time")
        }
        println("Have a good day!")
    }

    println()

    runBlocking {
        println("Weather forecast")
        println(getWeatherReportPartlyCancelled())
        println("Have a good day!")
    }

    println()
    println()

    runBlocking {
        println("${Thread.currentThread().name} - runBlocking function")
        launch {
            println("${Thread.currentThread().name} - launch function")
            withContext(Dispatchers.Default) {
                println("${Thread.currentThread().name} - withContext function")
                delay(1000)
                println("10 results found.")
            }
            println("${Thread.currentThread().name} - end of launch function")
        }
        println("Loading...")
    }

}

// suspend functions can only be called from other suspend functions
suspend fun printForecast() {
    delay(1000)
    println("Sunny")
}

suspend fun printTemperature() {
    delay(1000)
    println("30\u00b0C")
}

suspend fun getForecast(): String {
    delay(1000)
    return "Sunny"
}

suspend fun getTemperature(): String {
    delay(1000)
    return "30\u00b0C"
}

// coroutineScope returns when all work is done
// launch() and async() are extension functions on CoroutineScope
suspend fun getWeatherReport() = coroutineScope {
    val forecast = async { getForecast() }
    val temperature = async { getTemperature() }

    withContext(Dispatchers.Default){
        launch(Dispatchers.Default) {
            // 2 ways to chose Dispatcher (or other sing, see comment below
        }
    }

    "${forecast.await()} ${temperature.await()
    }"
}
// coroutine context, hold info, for example:
// name - name of the coroutine to uniquely identify it
// job - controls the lifecycle of the coroutine
// dispatcher - dispatches the work to the appropriate thread
// exception handler - handles exceptions thrown by the code executed in the coroutine

//Dispatchers.Main: Use this dispatcher to run a coroutine on the main Android thread. This dispatcher is used
//                  primarily for handling UI updates and interactions, and performing quick work.
//Dispatchers.IO: This dispatcher is optimized to perform disk or network I/O outside of the main thread.
//                For example, read from or write to files, and execute any network operations.
//Dispatchers.Default: This is a default dispatcher used when calling launch() and async(),
//                     when no dispatcher is specified in their context. You can use this dispatcher to
//                     perform computationally-intensive work outside of the main thread. For example,
//                     processing a bitmap image file.

suspend fun getWeatherReportWithError() = coroutineScope {
    throw AssertionError("Dikke error")
    val forecast = async { getForecast() }
    val temperature = async { getTemperature() }
    "${forecast.await()} ${temperature.await()}"
}

suspend fun getWeatherReportPartlyCancelled() = coroutineScope {
    val forecast = async { getForecast() }
    val temperature = async { getTemperature() }

    delay(200)
    temperature.cancel() //Cancelled!

    "${forecast.await()}"
}