package com.example.U2_PW3_ArtSpace_Project

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.U2_PW3_ArtSpace_Project.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ArtSpaceApp(Modifier)
                }
            }
        }
    }
}


@Composable
fun ArtSpaceApp(modifier: Modifier = Modifier) {
    var pageNumber by remember { mutableStateOf(0) }

    when (pageNumber) {
        0 -> ArtCanvas(
            description = R.string.description1,
            name = R.string.name1,
            year = R.string.year1,
            img = R.drawable.bird,
            onClickNext = {  pageNumber = nav(pageNumber, true) },
            onClickPrev = {  pageNumber = nav(pageNumber, false) }
        )

        1 -> ArtCanvas(
            description = R.string.description2,
            name = R.string.name2,
            year = R.string.year2,
            img = R.drawable.face,
            onClickNext = { pageNumber = nav(pageNumber, true) },
            onClickPrev = {  pageNumber = nav(pageNumber, false) }
        )

        else -> ArtCanvas(
            description = R.string.description3,
            name = R.string.name3,
            year = R.string.year3,
            img = R.drawable.eye,
            onClickNext = { pageNumber = nav(pageNumber, true) },
            onClickPrev = {  pageNumber = nav(pageNumber, false) }
        )
    }
}


@Composable
fun ArtCanvas(
    onClickPrev: () -> Unit,
    onClickNext: () -> Unit,
    @StringRes description: Int,
    @StringRes name: Int,
    @StringRes year: Int,
    @DrawableRes img: Int,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(20.dp),
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
        ArtworkWall(modifier = modifier.weight(1f), img = img)
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f),
            verticalArrangement = Arrangement.Bottom
        ) {
            ArtWorkDescriptor(
                modifier = modifier,
                description = description,
                name = name,
                year = year
            )
            Spacer(modifier = Modifier.height(24.dp))
            DisplayController(modifier = modifier, onClickPrev = onClickPrev , onClickNext = onClickNext)
        }

    }
}

@Composable
fun ArtworkWall(
    @DrawableRes img: Int,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier.padding(50.dp),
        shadowElevation = 11.dp
    ) {
        Image(
            modifier = Modifier
                .padding(20.dp),
            painter = painterResource(id = img),
            contentScale = ContentScale.Crop,
            contentDescription = null
        )
    }

}

@Composable
fun ArtWorkDescriptor(
    @StringRes description: Int,
    @StringRes name: Int,
    @StringRes year: Int,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier.fillMaxWidth(),
        color = MaterialTheme.colorScheme.secondaryContainer
    ) {
        Column(modifier = Modifier.padding(start = 8.dp, top = 15.dp, bottom = 15.dp)) {
            Text(
                text = stringResource(id = description),
                fontSize = 25.sp
            )
            Row(modifier = Modifier) {
                Text(text = stringResource(id = name), fontWeight = FontWeight.Bold)
                Spacer(modifier = Modifier.width(4.dp))
                Text(text = stringResource(id = year))
            }
        }

    }

}


@Composable
fun DisplayController(
    onClickPrev: () -> Unit,
    onClickNext: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Button(
            onClick = onClickPrev,
            modifier = Modifier.width(150.dp)
        ) {
            Text(text = "Previous")
        }
        Button(
            onClick = onClickNext,
            modifier = Modifier.width(150.dp)
        ) {
            Text(text = "Next")
        }
    }
}

private fun nav(number: Int, forward: Boolean): Int {
    Log.i("test","Clicked")
    var page: Int = if (forward) number + 1 else number - 1
    if (page > 2) page = 0
    if (page < 0) page = 2
    return page
}

@Preview(showBackground = true)
@Composable
fun ArtSpacePreview() {
    ArtSpaceTheme {
        ArtSpaceApp()
    }
}